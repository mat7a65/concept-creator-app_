# concept-creator-app - create awesome concepts

Within this longer lasting project I develop fullstack an application for developing and creating concepts in a structured process.
This application is based on a role- an permission-concept.

# Tech-Stack, Frameworks and Utilities:

## Backend
- Java
- Spring-Framework
- Spring-Security
- Hibernate
- MariaDB, HeidiSQL
- Maven
- JSON Web Token
- Spring Boot

## Frontend
- Angular 9
- TypeScript
- JSON, JWT
- HTML5
- CSS3
- Bootstrap

## Developed using:
- IntelliJ Idea
- Git, Gitlab


# Instructions and outlook:

In this application you can singn up and sign in as a user. Roles can be chanced manually 
or in a future version using the admin role.
Signed in as a creator, you can create awesome concepts and send them to the backendserver which stores them in a DB.
As a creator you can edit or delete your concepts. The concepts created by the creator could be sent to a decider, 
who can set the workflowstatus. He decides wheather the concepts could be implemented or not.

API's are protected using the role- and permission-based concept.

Possible developments handle peers who can vote for the concepts and give them credits. 
Incubators who can join concepts and give support as 'Business Angels'.

There are much more implementations conceivable. Let me know your ideas for further developments.