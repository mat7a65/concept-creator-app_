import {Component, OnInit} from '@angular/core';
import {UserService} from '../_services/user.service';
import {ConceptDTO} from '../_interfaces/conceptDTO';
import {HttpClient} from '@angular/common/http';
import {ConceptService} from '../_services/concept.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-board-creator',
  templateUrl: './board-creator.component.html',
  styleUrls: ['./board-creator.component.css']
})
export class BoardCreatorComponent implements OnInit {
  content = '';
  workflowStatus: number;
  concepts: ConceptDTO[];

  constructor(private userService: UserService, private conceptService: ConceptService, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.userService.getCreatorBoard().subscribe(
      data => {
        this.content = data;
        console.log(data);
      },
      err => {
        this.content = JSON.parse(err.error).message;
      }
    );

    this.conceptService.getAllMyConcepts().subscribe(
      conceptDTO => {
        console.log(conceptDTO);
        this.concepts = conceptDTO;
      });

    this.route.paramMap.subscribe(params =>
      this.workflowStatus = +params.get('workflowStatus'));
  }
}
