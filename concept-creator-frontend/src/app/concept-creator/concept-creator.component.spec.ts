import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConceptCreatorComponent } from './concept-creator.component';

describe('ConceptCreatorComponent', () => {
  let component: ConceptCreatorComponent;
  let fixture: ComponentFixture<ConceptCreatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConceptCreatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConceptCreatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
