import { Component, OnInit } from '@angular/core';
import {ConceptDTO} from '../_interfaces/conceptDTO';
import {HttpClient} from '@angular/common/http';
import {TokenStorageService} from '../_services/token-storage.service';
import {ConceptService} from '../_services/concept.service';
import {Router} from '@angular/router';

const CONCEPT_API = 'http://localhost:8080/api/concept/create';

@Component({
  selector: 'app-concept-creator',
  templateUrl: './concept-creator.component.html',
  styleUrls: ['./concept-creator.component.css']
})
export class ConceptCreatorComponent implements OnInit {

  username: string;
  isLoggedIn = false;

  conceptData: ConceptDTO = {
    id: 0,
    title: '',
    description: '',
    possibleReleaseDate: '',
    developmentStatus: '',
    minAmountperYear: 0,
    workflowStatus: 0,
  };

  status = ['Reseach', 'Development', 'Ready to market'];

  constructor(private http: HttpClient, private tokenStorageService: TokenStorageService,
              private conceptService: ConceptService, private router: Router) {
  }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();

    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.username = user.username;

    }
  }

  createConcept() {
    this.conceptService.createConcept(this.conceptData)
      .subscribe(response => console.log(response));
    this.router.navigate(['home']);
  }

}

