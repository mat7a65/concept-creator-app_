import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { BoardAdminComponent } from './board-admin/board-admin.component';
import { BoardUserComponent } from './board-user/board-user.component';
import { BoardDeciderComponent } from './board-decider/board-decider.component';
import { BoardCreatorComponent } from './board-creator/board-creator.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import { authInterceptorProviders } from './_helpers/auth.interceptor';
import { ConceptEditComponent } from './concept-edit/concept-edit.component';
import {ConceptCreatorComponent} from './concept-creator/concept-creator.component';
import { ConceptCoreComponent } from './concept-core/concept-core.component';
import { ConceptCoreDeciderComponent } from './concept-core-decider/concept-core-decider.component';
import { UserEditComponent } from './user-edit/user-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    ProfileComponent,
    BoardAdminComponent,
    BoardUserComponent,
    BoardDeciderComponent,
    BoardCreatorComponent,
    ConceptEditComponent,
    ConceptCreatorComponent,
    ConceptCoreComponent,
    ConceptCoreDeciderComponent,
    UserEditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [authInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
