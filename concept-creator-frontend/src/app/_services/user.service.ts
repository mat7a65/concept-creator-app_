import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {UserDTO} from '../_interfaces/userDTO';

const API_URL = 'api/test/';
const USER_API_READALL = 'api/admin/allusers';
const USER_API_READUSER = 'api/admin/readuser';
const USER_API_EDITUSER = 'api/admin/edituser';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {
  }

  getPublicContent(): Observable<any> {
    return this.http.get(API_URL + 'all', {responseType: 'text'});
  }

  getUserBoard(): Observable<any> {
    return this.http.get(API_URL + 'user', {responseType: 'text'});
  }

  getAdminBoard(): Observable<any> {
    return this.http.get(API_URL + 'admin', {responseType: 'text'});
  }

  getDeciderBoard(): Observable<any> {
    return this.http.get(API_URL + 'decider', {responseType: 'text'});
  }

  getCreatorBoard(): Observable<any> {
    return this.http.get(API_URL + 'creator', {responseType: 'text'});
  }

  getAllUsers(): Observable<UserDTO[]> {
    return this.http.get<UserDTO[]>(USER_API_READALL);
  }

  getUserById(userId: number): Observable<UserDTO> {
    return this.http.get<UserDTO>(USER_API_READUSER + '/' + userId);
  }

  editUser(userDTO: UserDTO): Observable<string> {
    return this.http.put<string>(USER_API_EDITUSER, userDTO);
  }
}
