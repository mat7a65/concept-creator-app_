import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ConceptDTO} from '../_interfaces/conceptDTO';
import {TokenStorageService} from './token-storage.service';
import {Observable} from 'rxjs';
import {Route} from '@angular/router';

const CONCEPT_API_READ = 'api/concept/read';
const CONCEPT_API_READ_WFSTATUS = 'api/concept/readwfstatus';
const CONCEPT_API_CREATE = 'api/concept/create';
const CONCEPT_API_EDIT = 'api/concept/edit';
const CONCEPT_API_SUBMITCONCEPT = 'api/concept/submitconcept';
const CONCEPT_API_REWORKCONCEPT = 'api/concept/reworkconcept';
const CONCEPT_API_DELETECONCEPT = 'api/concept/deleteconcept';

const CONCEPT_API_CHECKCONCEPT = 'api/concept/checkconcept';
const CONCEPT_API_ACCEPTCONCEPT = 'api/concept/acceptconcept';
const CONCEPT_API_DECLINECONCEPT = 'api/concept/declineconcept';

const CONCEPT_API_READ_DECIDER = 'api/concept/read/decider';

@Injectable({
  providedIn: 'root'
})
export class ConceptService {

  constructor(private http: HttpClient) {
  }

  getAllMyConcepts(): Observable<ConceptDTO[]> {
    return this.http.get<ConceptDTO[]>(CONCEPT_API_READ);
  }

  getMyConceptsByWfStatus(workflowStatus: number): Observable<ConceptDTO[]> {
    return this.http.get<ConceptDTO[]>(CONCEPT_API_READ_WFSTATUS + '/' + workflowStatus);
  }

  createConcept(conceptData: ConceptDTO): Observable<string> {
    return this.http.post<string>(CONCEPT_API_CREATE, conceptData);
  }

  getConceptById(conceptId: number): Observable<ConceptDTO> {
    return this.http.get<ConceptDTO>(CONCEPT_API_READ + '/' + conceptId);
  }

  editConcept(conceptData: ConceptDTO): Observable<string> {
    return this.http.put<string>(CONCEPT_API_EDIT, conceptData);
  }

  submitConcept(conceptDTO: ConceptDTO): Observable<string> {
    conceptDTO.workflowStatus = 1;
    return this.http.put<string>(CONCEPT_API_SUBMITCONCEPT, conceptDTO);
  }

  reworkConcept(conceptDTO: ConceptDTO): Observable<string> {
    conceptDTO.workflowStatus = 0;
    return this.http.put<string>(CONCEPT_API_REWORKCONCEPT, conceptDTO);
  }

  deleteConcept(conceptDTO: ConceptDTO): Observable<string> {
    conceptDTO.workflowStatus = 5;
    return this.http.delete<string>(CONCEPT_API_DELETECONCEPT + '/' + conceptDTO.id);
  }


  getAllConceptsByWfStatus(workflowStatus: number): Observable<ConceptDTO[]> {
    return this.http.get<ConceptDTO[]>(CONCEPT_API_READ_DECIDER + '/' + workflowStatus);
  }

  checkConcept(conceptDTO: ConceptDTO): Observable<string> {
    conceptDTO.workflowStatus = 2;
    return this.http.put<string>(CONCEPT_API_CHECKCONCEPT, conceptDTO);
  }

  acceptConcept(conceptDTO: ConceptDTO): Observable<string> {
    conceptDTO.workflowStatus = 3;
    return this.http.put<string>(CONCEPT_API_ACCEPTCONCEPT, conceptDTO);
  }

  declineConcept(conceptDTO: ConceptDTO): Observable<string> {
    conceptDTO.workflowStatus = 9;
    return this.http.put<string>(CONCEPT_API_DECLINECONCEPT, conceptDTO);
  }

}
