import {Component, OnChanges, OnInit} from '@angular/core';
import {UserService} from '../_services/user.service';
import {ConceptService} from '../_services/concept.service';
import {ConceptDTO} from '../_interfaces/conceptDTO';
import {ActivatedRoute} from '@angular/router';
import {TokenStorageService} from '../_services/token-storage.service';

@Component({
  selector: 'app-board-decider',
  templateUrl: './board-decider.component.html',
  styleUrls: ['./board-decider.component.css']
})
export class BoardDeciderComponent implements OnInit {
  content = '';
  concepts: ConceptDTO[];
  workflowStatus: number;
  constructor(private userService: UserService, private conceptService: ConceptService,
              private route: ActivatedRoute, private tokenStorageService: TokenStorageService) {
  }

  ngOnInit(): void {
    this.userService.getDeciderBoard().subscribe(
      data => {
        this.content = data;
      },
      err => {
        this.content = JSON.parse(err.error).message;
      }
    );

    this.route.paramMap.subscribe(params =>
      this.workflowStatus = +params.get('workflowStatus'));

  }
}
