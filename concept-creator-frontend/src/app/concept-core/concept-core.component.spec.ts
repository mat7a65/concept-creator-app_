import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConceptCoreComponent } from './concept-core.component';

describe('ConceptCoreComponent', () => {
  let component: ConceptCoreComponent;
  let fixture: ComponentFixture<ConceptCoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConceptCoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConceptCoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
