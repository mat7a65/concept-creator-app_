import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ConceptDTO} from '../_interfaces/conceptDTO';
import {UserService} from '../_services/user.service';
import {HttpClient} from '@angular/common/http';
import {ConceptService} from '../_services/concept.service';
import {TokenStorageService} from '../_services/token-storage.service';

@Component({
  selector: 'app-concept-core',
  templateUrl: './concept-core.component.html',
  styleUrls: ['./concept-core.component.css']
})
export class ConceptCoreComponent implements OnChanges {
  concepts: ConceptDTO[] = [];
  roles: string[] = [];

  constructor(private userService: UserService, private http: HttpClient,
              private conceptService: ConceptService, private tokenStorageService: TokenStorageService) {
  }

// Status: 0 = Draft, 1 = Submitted, 2 = In Examination, 3 = Accepted, 4 = Declined

  @Input()
  workflowStatus: number;


  ngOnChanges(): void {
    const user = this.tokenStorageService.getUser();
    this.roles = user.roles;
    this.conceptService.getMyConceptsByWfStatus(this.workflowStatus)
      .subscribe(conceptDTO => this.concepts = conceptDTO);
  }

  submitConcept(conceptDTO: ConceptDTO) {
    this.conceptService.submitConcept(conceptDTO)
      .subscribe(response => {
        console.log(response);
      });
  }

  reworkConcept(conceptDTO: ConceptDTO) {
    this.conceptService.reworkConcept(conceptDTO)
      .subscribe(response => {
        console.log(response);
      });
  }

  deleteConcept(conceptDTO: ConceptDTO) {
    this.conceptService.deleteConcept(conceptDTO)
      .subscribe(response => {
        console.log(response);
      });
  }
}
