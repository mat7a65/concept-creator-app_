import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConceptCoreDeciderComponent } from './concept-core-decider.component';

describe('ConceptCoreDeciderComponent', () => {
  let component: ConceptCoreDeciderComponent;
  let fixture: ComponentFixture<ConceptCoreDeciderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConceptCoreDeciderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConceptCoreDeciderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
