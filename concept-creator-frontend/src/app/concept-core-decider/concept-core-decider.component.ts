import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {UserService} from '../_services/user.service';
import {HttpClient} from '@angular/common/http';
import {ConceptService} from '../_services/concept.service';
import {TokenStorageService} from '../_services/token-storage.service';
import {ConceptDTO} from '../_interfaces/conceptDTO';

@Component({
  selector: 'app-concept-core-decider',
  templateUrl: './concept-core-decider.component.html',
  styleUrls: ['./concept-core-decider.component.css']
})
export class ConceptCoreDeciderComponent implements OnChanges {
  concepts: ConceptDTO[] = [];
  roles: string[];

  constructor(private userService: UserService, private http: HttpClient,
              private conceptService: ConceptService, private tokenStorageService: TokenStorageService) {
  }

  @Input()
  workflowStatus: number;

  ngOnChanges(): void {
    const user = this.tokenStorageService.getUser();
    this.roles = user.roles;

    if (this.workflowStatus !== 0) {
      this.conceptService.getAllConceptsByWfStatus(this.workflowStatus)
        .subscribe(conceptDTO => this.concepts = conceptDTO);
    }
  }

  checkConcept(conceptDTO: ConceptDTO) {
    this.conceptService.checkConcept(conceptDTO)
      .subscribe(response => {
        console.log(response);
      });
  }

  acceptConcept(conceptDTO: ConceptDTO) {
    this.conceptService.acceptConcept(conceptDTO)
      .subscribe(response => {
        console.log(response);
      });
  }

  declineConcept(conceptDTO: ConceptDTO) {
    this.conceptService.declineConcept(conceptDTO)
      .subscribe(response => {
        console.log(response);
      });
  }
}
