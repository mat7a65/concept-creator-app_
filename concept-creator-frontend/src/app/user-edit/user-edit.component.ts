import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {TokenStorageService} from '../_services/token-storage.service';
import {UserService} from '../_services/user.service';
import {UserDTO} from '../_interfaces/userDTO';
import {FormArray, FormBuilder, FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {

  form: FormGroup;
  rolesData = ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_CREATOR', 'ROLE_DECIDER'];

  userId: number;
  userData: UserDTO = {
    id: 0,
    username: '',
    email: '',
    roles: [],
  };

  constructor(private router: Router, private route: ActivatedRoute,
              private http: HttpClient, private tokenStorageService: TokenStorageService,
              private userService: UserService, private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({

      roles: new FormArray([])

    });

    this.addCheckboxes();
  }

  ngOnInit(): void {

    this.route.paramMap.subscribe(
      (params => {
        this.userId = +params.get('userId');
      }));

    this.userService.getUserById(this.userId)
      .subscribe(userDTO => {
        this.userData = userDTO;
        console.log(this.userData);
      });
  }

  get rolesFromArray() {
    return this.form.controls.roles as FormArray;
  }

  private addCheckboxes() {
    this.rolesData
      .forEach(
        () => this.rolesFromArray.push(
          new FormControl(false)));
  }

  editUser() {
    const selectedRoles = this.form.value.roles
      .map((checked, i) => checked ? this.rolesData[i] : null)
      .filter(v => v !== null);
    this.userData.roles = selectedRoles;
    this.userService.editUser(this.userData)
      .subscribe(response => console.log(response));
    this.router.navigate(['admin']);
  }
}
