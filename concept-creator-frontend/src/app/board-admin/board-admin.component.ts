import {Component, OnInit} from '@angular/core';
import {UserService} from '../_services/user.service';
import {UserDTO} from '../_interfaces/userDTO';
import {TokenStorageService} from '../_services/token-storage.service';

@Component({
  selector: 'app-board-admin',
  templateUrl: './board-admin.component.html',
  styleUrls: ['./board-admin.component.css']
})
export class BoardAdminComponent implements OnInit {
  content = '';
  userData: UserDTO[] = [];

  username: string;

  constructor(private userService: UserService, private tokenStorageService: TokenStorageService) {
  }

  ngOnInit(): void {
    this.userService.getAdminBoard().subscribe(
      data => {
        this.content = data;
      },
      err => {
        this.content = JSON.parse(err.error).message;
      }
    );

    this.userService.getAllUsers().subscribe(
      userDTO => {
        this.userData = userDTO;
        console.log(userDTO);
      }
    );

    const user = this.tokenStorageService.getUser();
    this.username = user.username;

  }
}
