import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {ProfileComponent} from './profile/profile.component';
import {BoardUserComponent} from './board-user/board-user.component';
import {BoardAdminComponent} from './board-admin/board-admin.component';
import {BoardCreatorComponent} from './board-creator/board-creator.component';
import {BoardDeciderComponent} from './board-decider/board-decider.component';
import {ConceptEditComponent} from './concept-edit/concept-edit.component';
import {ConceptCreatorComponent} from './concept-creator/concept-creator.component';
import {ConceptCoreComponent} from './concept-core/concept-core.component';
import {UserEditComponent} from './user-edit/user-edit.component';


const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'profile', component: ProfileComponent},
  {path: 'user', component: BoardUserComponent},
  {path: 'admin', component: BoardAdminComponent},
  {path: 'creator', component: BoardCreatorComponent},
  {path: 'decider', component: BoardDeciderComponent},
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'concept/create', component: ConceptCreatorComponent},
  {path: 'concept/edit/:conceptId', component: ConceptEditComponent},
  {path: 'creator/concepts/:workflowStatus', component: BoardCreatorComponent},
  {path: 'decider/concepts/:workflowStatus', component: BoardDeciderComponent},
  {path: 'admin/edituser/:userId', component: UserEditComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
