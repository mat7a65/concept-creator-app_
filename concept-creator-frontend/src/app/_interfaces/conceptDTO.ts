export interface ConceptDTO {
  id: number;
  title: string;
  description: string;
  possibleReleaseDate: string;
  developmentStatus: string;
  minAmountperYear: number;
  workflowStatus: number;
}
