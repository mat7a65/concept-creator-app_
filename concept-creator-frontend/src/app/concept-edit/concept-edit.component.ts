import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {TokenStorageService} from '../_services/token-storage.service';
import {ConceptService} from '../_services/concept.service';
import {ConceptDTO} from '../_interfaces/conceptDTO';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-concept-edit',
  templateUrl: './concept-edit.component.html',
  styleUrls: ['./concept-edit.component.css']
})
export class ConceptEditComponent implements OnInit {

  conceptId: number;

  conceptData: ConceptDTO = {
    id: 0,
    title: '',
    description: '',
    possibleReleaseDate: '',
    developmentStatus: '',
    minAmountperYear: 0,
    workflowStatus: 0,
  };

  status = ['Reseach', 'Development', 'Ready to market'];

  constructor(private router: Router, private route: ActivatedRoute,
              private http: HttpClient, private tokenStorageService: TokenStorageService,
              private conceptService: ConceptService) {
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(
      (params => {
        this.conceptId = +params.get('conceptId');
      })
    );

    this.conceptService.getConceptById(this.conceptId)
      .subscribe(conceptData => {
        this.conceptData = conceptData;
      });
  }

  editConcept() {
    this.conceptService.editConcept(this.conceptData).subscribe(
      response => {
        console.log(response);
      });
    this.router.navigate(['creator']);
  }
}
