package com.example.conceptcreatorbackend.service;

import com.example.conceptcreatorbackend.model.ERole;
import com.example.conceptcreatorbackend.model.Role;
import com.example.conceptcreatorbackend.model.User;
import com.example.conceptcreatorbackend.payload.UserDTO;
import com.example.conceptcreatorbackend.repository.RoleRepository;
import com.example.conceptcreatorbackend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    @Autowired
    public UserService(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    public List<UserDTO> getAllUsers() {
        List<UserDTO> userDTOList = new ArrayList<>();
        userRepository.findAll().forEach(user ->
                {
                    userDTOList.add(
                            new UserDTO(
                                    user.getId(),
                                    user.getUsername(),
                                    user.getEmail(),
                                    user.getRoles()
                                            .stream()
                                            .map(role -> role.getName().name())
                                            .collect(Collectors.toSet())
                            )
                    );
                }
        );
        return userDTOList;
    }

    public UserDTO getUserById(Long id) {
        Optional<User> optionalUser = userRepository.findUserById(id);
        User user = new User();

        if(optionalUser.isPresent()) {
            user = optionalUser.get();
        }

        return new UserDTO(user.getId(), user.getUsername(), user.getEmail(),
                user.getRoles().stream()
                        .map(role -> role.getName().name())
                        .collect(Collectors.toSet()));
    }

    public void editUser(UserDTO userDTO) {
        User user = new User();
        Optional<User> optionalUser = userRepository.findUserById(userDTO.getId());
        if(optionalUser.isPresent()) {
            user = optionalUser.get();
        }

        Set<ERole> eRoleSet = new HashSet<>();
        userDTO.getRoles().forEach(role -> {
            eRoleSet.add(ERole.valueOf(role));
        });

        Set<Role> roleSet = new HashSet<>();

        eRoleSet.forEach(eRole -> {
            Optional<Role> optionalRole = roleRepository.findByName(eRole);

            if(optionalRole.isPresent()) {
                roleSet.add(optionalRole.get());
            }

        });
        user.setRoles(roleSet);
        userRepository.save(user);
    }
}
