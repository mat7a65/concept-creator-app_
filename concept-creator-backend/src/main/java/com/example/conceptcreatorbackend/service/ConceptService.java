package com.example.conceptcreatorbackend.service;

import com.example.conceptcreatorbackend.model.Concept;
import com.example.conceptcreatorbackend.model.User;
import com.example.conceptcreatorbackend.payload.ConceptDTO;
import com.example.conceptcreatorbackend.payload.response.MessageResponse;
import com.example.conceptcreatorbackend.repository.ConceptRepository;
import com.example.conceptcreatorbackend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ConceptService {

    private final ConceptRepository conceptRepository;
    private final UserRepository userRepository;

    @Autowired
    public ConceptService(ConceptRepository conceptRepository, UserRepository userRepository) {
        this.conceptRepository = conceptRepository;
        this.userRepository = userRepository;
    }

    public List<ConceptDTO> getAllMyConcepts() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<Concept> concepts = conceptRepository.findAllByUser_username(userDetails.getUsername());
        List<ConceptDTO> conceptDTOList = new ArrayList<>();
        concepts.forEach(concept -> {
            conceptDTOList.add(
                    new ConceptDTO(
                            concept.getConceptId(),
                            concept.getTitle(),
                            concept.getDescription(),
                            concept.getPossibleReleaseDate(),
                            concept.getDevelopmentStatus(),
                            concept.getMinAmountperYear(),
                            concept.getWorkflowStatus()
                    ));
        });
        return conceptDTOList;
    }


    public void addConcept(ConceptDTO conceptDTO) {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User testUser = new User();

        Optional<User> optionalUser = userRepository.findByUsernameIgnoreCase(userDetails.getUsername());
        if (optionalUser.isPresent()) {
            testUser = optionalUser.get();
        }

        Concept concept = new Concept(
                conceptDTO.getTitle(),
                conceptDTO.getDescription(),
                conceptDTO.getPossibleReleaseDate(),
                conceptDTO.getDevelopmentStatus(),
                conceptDTO.getMinAmountperYear(),
                testUser

        );
//        Test Workflowstatus
        concept.setWorkflowStatus(0);
        conceptRepository.save(concept);
    }

    public ConceptDTO getConcept(int conceptId) {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Concept concept = new Concept();
        Optional<Concept> optionalConcept = conceptRepository.findConceptByConceptIdAndUser_Username(conceptId, userDetails.getUsername());
        if (optionalConcept.isPresent()) {
            concept = optionalConcept.get();
        }

        return new ConceptDTO(
                concept.getConceptId(),
                concept.getTitle(),
                concept.getDescription(),
                concept.getPossibleReleaseDate(),
                concept.getDevelopmentStatus(),
                concept.getMinAmountperYear(),
                concept.getWorkflowStatus()
        );
    }

    public void editConcept(ConceptDTO conceptDTO) {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Concept concept = new Concept();
        Optional<Concept> optionalConcept = conceptRepository.findConceptByConceptIdAndUser_Username(
                conceptDTO.getId(), userDetails.getUsername());

        if (optionalConcept.isPresent()) {
            concept = optionalConcept.get();
        }

        concept.setTitle(conceptDTO.getTitle());
        concept.setDescription(concept.getDescription());
        concept.setPossibleReleaseDate(conceptDTO.getPossibleReleaseDate());
        concept.setDevelopmentStatus(conceptDTO.getDevelopmentStatus());
        concept.setMinAmountperYear(conceptDTO.getMinAmountperYear());
        conceptRepository.save(concept);
    }

    public void setWorkflowstatusCreator(ConceptDTO conceptDTO) {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Concept concept = new Concept();
        Optional<Concept> optionalConcept = conceptRepository.findConceptByConceptIdAndUser_Username(
                conceptDTO.getId(), userDetails.getUsername());

        if (optionalConcept.isPresent()) {
            concept = optionalConcept.get();
        }

        concept.setTitle(conceptDTO.getTitle());
        concept.setDescription(concept.getDescription());
        concept.setPossibleReleaseDate(conceptDTO.getPossibleReleaseDate());
        concept.setDevelopmentStatus(conceptDTO.getDevelopmentStatus());
        concept.setMinAmountperYear(conceptDTO.getMinAmountperYear());
        concept.setWorkflowStatus(conceptDTO.getWorkflowStatus());
        conceptRepository.save(concept);
    }

    public void setWorkflowstatusDecider(ConceptDTO conceptDTO) {

        Concept concept = new Concept();
        Optional<Concept> optionalConcept = conceptRepository.findConceptByConceptId(conceptDTO.getId());

        if (optionalConcept.isPresent()) {
            concept = optionalConcept.get();
        }

        concept.setTitle(conceptDTO.getTitle());
        concept.setDescription(concept.getDescription());
        concept.setPossibleReleaseDate(conceptDTO.getPossibleReleaseDate());
        concept.setDevelopmentStatus(conceptDTO.getDevelopmentStatus());
        concept.setMinAmountperYear(conceptDTO.getMinAmountperYear());
        concept.setWorkflowStatus(conceptDTO.getWorkflowStatus());
        conceptRepository.save(concept);
    }


    public List<ConceptDTO> getConceptsByWorkflowStatus(int workflowStatus) {
        List<ConceptDTO> conceptDTOList = new ArrayList<>();
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        conceptRepository.findConceptByWorkflowStatusAndUser_Username(workflowStatus, userDetails.getUsername())
                .forEach(concept -> {
                    conceptDTOList.add(
                            new ConceptDTO(
                                    concept.getConceptId(),
                                    concept.getTitle(),
                                    concept.getDescription(),
                                    concept.getPossibleReleaseDate(),
                                    concept.getDevelopmentStatus(),
                                    concept.getMinAmountperYear(),
                                    concept.getWorkflowStatus()
                            )
                    );
                });
        return conceptDTOList;
    }

    public void deleteConcept(int conceptID) {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Concept concept = new Concept();
        Optional<Concept> optionalConcept = conceptRepository.findConceptByConceptIdAndUser_Username(
                conceptID, userDetails.getUsername());

        if (optionalConcept.isPresent()) {
            concept = optionalConcept.get();
        }

        conceptRepository.delete(concept);
    }


    public List<ConceptDTO> getMyWorkloadDecider(int workflowstatus) {
        List<Concept> conceptList = conceptRepository.findConceptsByWorkflowStatus(workflowstatus);
        List<ConceptDTO> conceptDTOList = new ArrayList<>();
        conceptList.forEach(concept -> {
            conceptDTOList.add(
                    new ConceptDTO(
                            concept.getConceptId(),
                            concept.getTitle(),
                            concept.getDescription(),
                            concept.getPossibleReleaseDate(),
                            concept.getDevelopmentStatus(),
                            concept.getMinAmountperYear(),
                            concept.getWorkflowStatus()
                    ));
        });
        return conceptDTOList;
    }
}
