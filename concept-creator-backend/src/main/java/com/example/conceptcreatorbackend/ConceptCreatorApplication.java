package com.example.conceptcreatorbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConceptCreatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConceptCreatorApplication.class, args);



	}

}
