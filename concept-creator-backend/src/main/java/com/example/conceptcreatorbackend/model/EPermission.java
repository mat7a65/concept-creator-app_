package com.example.conceptcreatorbackend.model;

public enum EPermission {
    CONCEPT_READ,
    CONCEPT_WRITE,
    CONCEPT_EDIT,
    CONCEPT_DELETE,
    CONCEPT_SETWFSTATUS,
    CONCEPT_JOIN,
    CONCEPT_VOTE,

    USER_READ,
    USER_WRITE,
    USER_EDIT,
    USER_SETROLE,
    USER_DELETE;
}
