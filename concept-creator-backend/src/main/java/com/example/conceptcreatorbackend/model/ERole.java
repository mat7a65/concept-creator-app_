package com.example.conceptcreatorbackend.model;

public enum ERole {
    ROLE_USER,
    ROLE_ADMIN,
    ROLE_CREATOR,
    ROLE_DECIDER;
}
