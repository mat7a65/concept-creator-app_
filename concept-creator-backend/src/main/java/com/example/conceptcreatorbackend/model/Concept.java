package com.example.conceptcreatorbackend.model;

import javax.persistence.*;

@Entity
public class Concept {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int conceptId;

    private String title;
    private String description;
    private String possibleReleaseDate;
    private String developmentStatus;
    private int minAmountperYear;
    private int workflowStatus;

    @ManyToOne
    private User user;

    public Concept() {
    }

    public Concept(String title, String description, String possibleReleaseDate,
                   String developmentStatus, int minAmountperYear, User user) {
        this.title = title;
        this.description = description;
        this.possibleReleaseDate = possibleReleaseDate;
        this.developmentStatus = developmentStatus;
        this.minAmountperYear = minAmountperYear;
        this.user = user;
    }


    public int getConceptId() {
        return conceptId;
    }

    public void setConceptId(int conceptId) {
        this.conceptId = conceptId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPossibleReleaseDate() {
        return possibleReleaseDate;
    }

    public void setPossibleReleaseDate(String possibleReleaseDate) {
        this.possibleReleaseDate = possibleReleaseDate;
    }

    public String getDevelopmentStatus() {
        return developmentStatus;
    }

    public void setDevelopmentStatus(String developmentStatus) {
        this.developmentStatus = developmentStatus;
    }

    public int getMinAmountperYear() {
        return minAmountperYear;
    }

    public void setMinAmountperYear(int minAmountperYear) {
        this.minAmountperYear = minAmountperYear;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getWorkflowStatus() {
        return workflowStatus;
    }

    public void setWorkflowStatus(int workflowStatus) {
        this.workflowStatus = workflowStatus;
    }
}
