package com.example.conceptcreatorbackend.repository;

import com.example.conceptcreatorbackend.model.ERole;
import com.example.conceptcreatorbackend.model.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends CrudRepository<Role, Integer> {

    Optional<Role> findByName(ERole name);
}
