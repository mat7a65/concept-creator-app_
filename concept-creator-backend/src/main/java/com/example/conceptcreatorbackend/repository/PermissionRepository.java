package com.example.conceptcreatorbackend.repository;

import com.example.conceptcreatorbackend.model.EPermission;
import com.example.conceptcreatorbackend.model.Permission;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PermissionRepository extends CrudRepository<Permission, Integer> {

    Optional<Permission> findByName(EPermission name);

}
