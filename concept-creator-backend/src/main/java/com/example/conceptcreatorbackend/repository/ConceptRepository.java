package com.example.conceptcreatorbackend.repository;

import com.example.conceptcreatorbackend.model.Concept;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ConceptRepository extends CrudRepository<Concept, Integer> {

    List<Concept> findAllByUser_username(String username);

    Optional<Concept> findByUser_usernameIgnoreCase(String username);

    Optional<Concept> findConceptByConceptIdAndUser_Username(int id, String username);

    Optional<Concept> findConceptByConceptId(int id);

    List<Concept> findConceptByWorkflowStatusAndUser_Username(int workflowStatus, String username);

    List<Concept> findConceptsByWorkflowStatus(int workflowstatus);
}
