package com.example.conceptcreatorbackend;

import com.example.conceptcreatorbackend.model.EPermission;
import com.example.conceptcreatorbackend.model.ERole;
import com.example.conceptcreatorbackend.model.Permission;
import com.example.conceptcreatorbackend.model.Role;
import com.example.conceptcreatorbackend.repository.PermissionRepository;
import com.example.conceptcreatorbackend.repository.RoleRepository;
import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.example.conceptcreatorbackend.model.EPermission.*;

@Component
public class SetupComponent {
    private final RoleRepository roleRepository;
    private final PermissionRepository permissionRepository;

    @Autowired
    public SetupComponent(RoleRepository roleRepository, PermissionRepository permissionRepository) {
        this.roleRepository = roleRepository;
        this.permissionRepository = permissionRepository;
    }

    @EventListener
    @Transactional
    public void handleApplicationReady(ApplicationReadyEvent event) {


        if (roleRepository.count() == 0) {

            // Generate the PERMISSIONS
            generatePermissions(Sets.newHashSet(
                    CONCEPT_READ,
                    CONCEPT_WRITE,
                    CONCEPT_EDIT,
                    CONCEPT_DELETE,
                    CONCEPT_SETWFSTATUS,
                    CONCEPT_JOIN,
                    CONCEPT_VOTE,

                    USER_READ,
                    USER_WRITE,
                    USER_EDIT,
                    USER_SETROLE,
                    USER_DELETE
            ));


            // Generate the ROLES and set the Permissions, save them into the repos

            // User
            generateRole(ERole.ROLE_USER);
            setPermissions(ERole.ROLE_USER, Sets.newHashSet(CONCEPT_READ, CONCEPT_VOTE, USER_READ));

            // Admin
            generateRole(ERole.ROLE_ADMIN);
            setPermissions(ERole.ROLE_ADMIN, Sets.newHashSet(
                    CONCEPT_READ,
                    CONCEPT_WRITE,
                    CONCEPT_EDIT,
                    CONCEPT_DELETE,
                    CONCEPT_SETWFSTATUS,
                    CONCEPT_JOIN,
                    CONCEPT_VOTE,
                    USER_READ,
                    USER_WRITE,
                    USER_EDIT,
                    USER_SETROLE,
                    USER_DELETE
            ));

            // Creator
            generateRole(ERole.ROLE_CREATOR);
            setPermissions(ERole.ROLE_CREATOR, Sets.newHashSet(
                    CONCEPT_READ,
                    CONCEPT_WRITE,
                    CONCEPT_EDIT,
                    CONCEPT_DELETE,
                    USER_READ,
                    USER_EDIT
            ));

            // Decider
            generateRole(ERole.ROLE_DECIDER);
            setPermissions(ERole.ROLE_DECIDER, Sets.newHashSet(
                    CONCEPT_READ,
                    CONCEPT_SETWFSTATUS,
                    USER_READ,
                    USER_EDIT
            ));

        }
    }


    private void generatePermissions(Set<EPermission> ePermissions) {
         Set<Permission> permissions = ePermissions.stream()
                .map(ePermission -> new Permission(ePermission))
                .collect(Collectors.toSet());
         permissionRepository.saveAll(permissions);
    }

    private void generateRole(ERole eRole) {
        roleRepository.save(new Role(eRole));
    }

    private void setPermissions(ERole eRole, Set<EPermission> ePermission) {
        Role role = roleRepository.findByName(eRole).get();

        Set<Permission> permissions = ePermission.stream()
                .map(p -> permissionRepository.findByName(p).get())
                .collect(Collectors.toSet());

        role.setPermissions(permissions);
    }
}



