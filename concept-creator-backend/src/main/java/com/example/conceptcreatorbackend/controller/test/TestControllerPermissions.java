package com.example.conceptcreatorbackend.controller.test;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/test")
public class TestControllerPermissions {

    @GetMapping("/concepttest")
    @PreAuthorize("hasAnyAuthority('CONCEPT_READ', 'CONCEPT_WRITE', 'CONCEPT_EDIT', 'CONCEPT_DELETE'," +
            " 'CONCEPT_SETWFSTATUS', 'CONCEPT_JOIN', 'CONCEPT_VOTE')")
    public String conceptTestAccess() {
        return "Permission concept.";
    }


    @GetMapping("/usertest")
    @PreAuthorize("hasAnyAuthority('USER_READ', 'USER_WRITE', 'USER_EDIT', 'USER_SETROLE', 'USER_DELETE')")
    public String userTestAccess() {
        return "Permission user.";
    }

}
