package com.example.conceptcreatorbackend.controller;

import com.example.conceptcreatorbackend.payload.ConceptDTO;
import com.example.conceptcreatorbackend.payload.response.MessageResponse;
import com.example.conceptcreatorbackend.service.ConceptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/concept")
public class ConceptController {

    private ConceptService conceptService;

    @Autowired
    public ConceptController(ConceptService conceptService) {
        this.conceptService = conceptService;
    }

    @GetMapping("/read")
    @PreAuthorize("hasAuthority('CONCEPT_READ')")
    public List<ConceptDTO> getAllMyConcepts() {
        return conceptService.getAllMyConcepts();
    }

    @GetMapping("/read/{conceptId}")
    @PreAuthorize("hasAuthority('CONCEPT_WRITE')")
    public ConceptDTO getConceptById(@PathVariable int conceptId) {
        return conceptService.getConcept(conceptId);
    }

    @GetMapping("/readwfstatus/{workflowStatus}")
    @PreAuthorize("hasAuthority('CONCEPT_READ')")
    public List<ConceptDTO> getConceptsByWorkflowStatus(@PathVariable int workflowStatus) {
        return conceptService.getConceptsByWorkflowStatus(workflowStatus);
    }

    @PostMapping("/create")
    @PreAuthorize("hasAuthority('CONCEPT_WRITE')")
    public ResponseEntity<?> createConcept(@RequestBody ConceptDTO conceptDTO) {
        conceptService.addConcept(conceptDTO);
        return ResponseEntity.ok(new MessageResponse("Concept stored successfully!"));
    }

    @PutMapping("/edit")
    @PreAuthorize("hasAuthority('CONCEPT_WRITE')")
    public ResponseEntity<?> editConcept(@RequestBody ConceptDTO conceptDTO) {
        conceptService.editConcept(conceptDTO);
        return ResponseEntity.ok(new MessageResponse("Concept edited successfully!"));
    }

    @PutMapping("/submitconcept")
    @PreAuthorize("hasRole('CREATOR')")
    public ResponseEntity<?> submitConcept(@RequestBody ConceptDTO conceptDTO) {
        conceptService.setWorkflowstatusCreator(conceptDTO);
        return ResponseEntity.ok(new MessageResponse("Action performed successfully!" +
                "-> setStatus = 1/Submitted"));
    }

    @PutMapping("/reworkconcept")
    @PreAuthorize("hasRole('CREATOR')")
    public ResponseEntity<?> reworkConcept(@RequestBody ConceptDTO conceptDTO) {
        conceptService.setWorkflowstatusCreator(conceptDTO);
        return ResponseEntity.ok(new MessageResponse("Action performed successfully! " +
                "-> setStatus = 0/Draft"));
    }

    @DeleteMapping("/deleteconcept/{conceptId}")
    @PreAuthorize("hasRole('CREATOR')")
    public ResponseEntity<?> deleteConcept(@PathVariable int conceptId) {
        conceptService.deleteConcept(conceptId);
        return ResponseEntity.ok(new MessageResponse("Concept deleted successfully!"));
    }

    @GetMapping("/read/decider/{workflowStatus}")
    @PreAuthorize("hasAuthority('CONCEPT_READ')")
    public List<ConceptDTO> getMyWorkload(@PathVariable int workflowStatus) {
        return conceptService.getMyWorkloadDecider(workflowStatus);
    }

    @PutMapping("/checkconcept")
    @PreAuthorize("hasRole('DECIDER')")
    public ResponseEntity<?> checkConcept(@RequestBody ConceptDTO conceptDTO) {
        conceptService.setWorkflowstatusDecider(conceptDTO);
        return ResponseEntity.ok(new MessageResponse("Action performed successfully!" +
                "-> setStatus = 2/In Examination"));
    }

    @PutMapping("/acceptconcept")
    @PreAuthorize("hasRole('DECIDER')")
    public ResponseEntity<?> acceptConcept(@RequestBody ConceptDTO conceptDTO) {
        conceptService.setWorkflowstatusDecider(conceptDTO);
        return ResponseEntity.ok(new MessageResponse("Action performed successfully!" +
                "-> setStatus = 3/Accepted"));
    }

    @PutMapping("/declineconcept")
    @PreAuthorize("hasRole('DECIDER')")
    public ResponseEntity<?> declineConcept(@RequestBody ConceptDTO conceptDTO) {
        conceptService.setWorkflowstatusDecider(conceptDTO);
        return ResponseEntity.ok(new MessageResponse("Action performed successfully!" +
                "-> setStatus = 9/Declined"));
    }
}
