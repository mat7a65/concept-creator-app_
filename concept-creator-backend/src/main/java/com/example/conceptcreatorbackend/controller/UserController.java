package com.example.conceptcreatorbackend.controller;

import com.example.conceptcreatorbackend.payload.UserDTO;
import com.example.conceptcreatorbackend.payload.response.MessageResponse;
import com.example.conceptcreatorbackend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/allusers")
    @PreAuthorize("hasRole('ADMIN')")
    public List<UserDTO> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/readuser/{userId}")
    @PreAuthorize("hasRole('ADMIN')")
    public UserDTO getUserById(@PathVariable Long userId) {
        return userService.getUserById(userId);
    }

    @PutMapping("/edituser")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> editUser(@RequestBody UserDTO userDTO) {
        userService.editUser(userDTO);
        return ResponseEntity.ok(new MessageResponse("Action performed successfully!" +
                "-> edited users role"));
    }
}
