package com.example.conceptcreatorbackend.controller.test;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/test")
public class TestControllerRoles {

    @GetMapping("/all")
    public String allAccess() {
        return "Public Content.";
    }

    @GetMapping("/user")
    @PreAuthorize("hasRole('USER')")
    public String userAccess() {
        return "User Content.";
    }

    @GetMapping("/admin")
    @PreAuthorize("hasRole('ADMIN')")
    public String adminAccess() {
        return "Admin Board.";
    }

    @GetMapping("/creator")
    @PreAuthorize("hasRole('CREATOR')")
    public String creatorAccess() {
        return "Creator Board.";
    }

    @GetMapping("/decider")
    @PreAuthorize("hasRole('DECIDER')")
    public String deciderAccess() {
        return "Decider Board.";
    }

}
