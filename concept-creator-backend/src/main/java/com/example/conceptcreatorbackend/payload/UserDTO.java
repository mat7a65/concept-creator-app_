package com.example.conceptcreatorbackend.payload;

import java.util.Set;

public class UserDTO {

    private long id;
    private String username;
    private String email;
    private Set<String> roles;

    public UserDTO(long id, String name, String email, Set<String> roles) {
        this.id = id;
        this.username = name;
        this.email = email;
        this.roles = roles;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }
}
