package com.example.conceptcreatorbackend.payload;

public class ConceptDTO {

    private int id;
    private String title;
    private String description;
    private String possibleReleaseDate;
    private String developmentStatus;
    private int minAmountperYear;
    private int workflowStatus;

    public ConceptDTO() {
    }

    public ConceptDTO(int id, String title, String description, String possibleReleaseDate,
                      String developmentStatus, int minAmountperYear, int workflowStatus) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.possibleReleaseDate = possibleReleaseDate;
        this.developmentStatus = developmentStatus;
        this.minAmountperYear = minAmountperYear;
        this.workflowStatus = workflowStatus;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPossibleReleaseDate() {
        return possibleReleaseDate;
    }

    public void setPossibleReleaseDate(String possibleReleaseDate) {
        this.possibleReleaseDate = possibleReleaseDate;
    }

    public String getDevelopmentStatus() {
        return developmentStatus;
    }

    public void setDevelopmentStatus(String developmentStatus) {
        this.developmentStatus = developmentStatus;
    }

    public int getMinAmountperYear() {
        return minAmountperYear;
    }

    public void setMinAmountperYear(int minAmountperYear) {
        this.minAmountperYear = minAmountperYear;
    }

    public int getWorkflowStatus() {
        return workflowStatus;
    }

    public void setWorkflowStatus(int workflowStatus) {
        this.workflowStatus = workflowStatus;
    }
}
